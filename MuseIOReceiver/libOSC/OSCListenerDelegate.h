//
//  OSCListenerDelegate.h
//  smoke_iphone
//
//  Created by Field Temple on 11-03-22.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

@protocol  OSCListenerDelegate

-(void) ProcessMessage: (const osc::ReceivedMessage &) m;

@end