//
//  ILLPCocoaOSCPack.m
//  cocoaoscpack
//
//  Created by C. Ramakrishnan on 09.03.08.
//  Copyright 2008 Illposed Software. All rights reserved.
//

#import "ILLPCocoaOSCPack.h"

//  For OSC
#include "OscReceivedElements.h"
#include "OscPacketListener.h"
#include "OscOutboundPacketStream.h"
#include "IpEndpointName.h"
#include <netinet/in.h>

//  For std:: output
#include <iostream>
#include "OscPrintReceivedElements.h"

//  For Networking
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>


@interface ILLPCocoaOSCPack (ILLPCocoaOSCPackPrivate)
-(void)initializeOSCState;
- (void)clearMessageData;
@end

@implementation ILLPCocoaOSCPack
#pragma mark -
#pragma mark NSObject Overrides
- (void)dealloc
{
	delete mOSCListener;
	if (mOSCReceiverAddressData) CFRelease(mOSCReceiverAddressData);
	if (mMessageData) CFRelease(mMessageData);
}

//  in other cases, init gets called
- (id)init
{
	if (!(self = [super init])) return nil;
	
	//[self initializeOSCState: 57130 outboundIP: @"127.0.0.1" outboundPort: 57120];
	
	return self;
}


- (float)fooValue { return mFooValue; }

- (void)setFooValue:(float)fooValue 
{
	mFooValue = fooValue;

	[self clearMessageData];
	
	osc::OutboundPacketStream p(mMessageBuffer, IP_MTU_SIZE);
	p.Clear();
	
	p << osc::BeginMessage("/foo") << (float) fooValue << osc::EndMessage;
	CFDataAppendBytes(mMessageData, (const UInt8*) p.Data(), p.Size());
	CFSocketError err = CFSocketSendData(mOSCListener->GetSocket(), mOSCReceiverAddressData, mMessageData, 0.1);
	if (kCFSocketSuccess != err) {
		NSLog(@"Failed to send foo value %li", err);
	}
}

#pragma mark -
#pragma mark ILLPCocoaOSCPackPrivate
-(void) initializeOSCStateWithInboundPort: (int) inboundPort outboundIP: (NSString *) outboundIP outboundPort: (int) outboundPort
{
	mOSCListener = new ILLPOSCListener();
	mOSCListener->SetSocketPort(inboundPort);
	
	struct sockaddr_in addr;
	
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr([outboundIP UTF8String]);
	// SuperCollider Language
	addr.sin_port = htons(outboundPort);
	
	if (mOSCReceiverAddressData) CFRelease(mOSCReceiverAddressData), mOSCReceiverAddressData = NULL;
	mOSCReceiverAddressData = CFDataCreate(NULL,  (UInt8 *)&addr, sizeof(struct sockaddr_in));
	
	if (mMessageData) CFRelease(mMessageData), mMessageData = NULL;
	mMessageData = CFDataCreateMutable(NULL, IP_MTU_SIZE);
}

- (void)clearMessageData
{
	CFRange range = CFRangeMake(0, CFDataGetLength(mMessageData));
	CFDataDeleteBytes(mMessageData, range);
}


@end
