//
//  ILLPCocoaOSCPack.h
//  cocoaoscpack
//
//  Created by C. Ramakrishnan on 09.03.08.
//  Copyright 2008 Illposed Software. All rights reserved.
//

//  Useful Defines
#ifdef __cplusplus
#  define ZKMOR_C_BEGIN extern "C" {
#  define ZKMOR_C_END   }
#  define ZKMCPPT(objtype) objtype*
#  define ZKMDECLCPPT(decl) class decl;
#else
#  define ZKMOR_C_BEGIN
#  define ZKMOR_C_END
#  define ZKMCPPT(objtype) void*
#  define ZKMDECLCPPT(decl) 
#endif

#import <Foundation/Foundation.h>
#include "ILLPOSCListener.h"
#import "Router.h"

class ILLPOSCListener;
//ZKMDECLCPPT(ILLPOSCListener)

// For OSC Output
#define IP_MTU_SIZE 1536
@interface ILLPCocoaOSCPack : Router {
	// OSC State
	ZKMCPPT(ILLPOSCListener)	mOSCListener;
	CFDataRef					mOSCReceiverAddressData;
	CFMutableDataRef			mMessageData;
	CFIndex						mMessageDataSize;
	char						mMessageBuffer[IP_MTU_SIZE];

	//  App State
	float						mFooValue;
}

- (float)fooValue;
- (void)setFooValue:(float)fooValue;

-(void) initializeOSCStateWithInboundPort: (int) inboundPort outboundIP: (NSString *) outboundIP outboundPort: (int) outboundPort;
-(void) clearMessageData;

@end
