/*
 *  ILLPOSCListener.cpp
 *  smoke_iphone
 *
 *  Created by Field Temple on 11-03-21.
 *  Copyright 2011 InteraXon. All rights reserved.
 *
 */

#include "ILLPOSCListener.h"
#include "OscReceivedElements.h"

ILLPOSCListener::ILLPOSCListener(void) : OscPacketListener(), mSocket(NULL), mIsSocketInitialized(false), mInputSocketRunLoopSource(NULL)
{ 
	mDelegate = nil;
	CreateSocket();
}

ILLPOSCListener::~ILLPOSCListener()
{
	if (mSocket) DeleteInputSocket();
	if (mInputSocketRunLoopSource) CFRelease(mInputSocketRunLoopSource);
}

//  Accessors
void ILLPOSCListener::SetSocketPort(short portNumber)
{
	// Can't change the address of a socket, so we need to reallocate
	if (mIsSocketInitialized) { DeleteInputSocket(); CreateSocket(); }
	
	mInputPortNumber = portNumber;
	struct sockaddr_in addr;
	
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);	
	addr.sin_port = htons(portNumber);
	
	CFDataRef addressData = CFDataCreateWithBytesNoCopy(NULL,  (UInt8 *)&addr, sizeof(struct sockaddr_in), kCFAllocatorNull);
	CFSocketError error = CFSocketSetAddress(mSocket, addressData);
	
	if (kCFSocketSuccess != error) {
		if (kCFSocketError == error) {
			NSLog(@"kSocketErr err %i", errno);
			NSDictionary* userInfo = 
			[NSDictionary 
			 dictionaryWithObjectsAndKeys: 
			 @"Could not start OSC listener.", NSLocalizedDescriptionKey, 
			 @"Could not set addr for OSC listener socket: kCFSocketError.", NSLocalizedFailureReasonErrorKey, nil];
			NSError* errorObject = [NSError errorWithDomain: NSPOSIXErrorDomain  code: error userInfo: userInfo];
			NSLog(@"error: %@", errorObject);
			//[[UIApplication sharedApplication] presentError: errorObject];
		} else {
			NSLog(@"kCFSocketTimeout err %i", errno);
			NSDictionary* userInfo = 
			[NSDictionary 
			 dictionaryWithObjectsAndKeys: 
			 @"Could not start OSC listener.", NSLocalizedDescriptionKey, 
			 @"Could not set addr for OSC listener socket: kCFSocketTimeout.", NSLocalizedFailureReasonErrorKey, nil];
			NSError* errorObject = [NSError errorWithDomain: NSPOSIXErrorDomain  code: error userInfo: userInfo];
			NSLog(@"error: %@", errorObject);
			//[[UIApplication sharedApplication] presentError: errorObject];
		}
	}
	
	CFRelease(addressData);
	mIsSocketInitialized = true;
}

void ILLPOSCListener::InputSocketCallback(CFSocketRef s, CFSocketCallBackType callbackType, CFDataRef address, const void *primData, void *info)
{
	CFDataRef data = reinterpret_cast<CFDataRef>(primData);
	// convert data to a const char* and int size;
	const char* bytes = (const char*) CFDataGetBytePtr(data);
	int bytesSize = CFDataGetLength(data);
	
	// convert the address to a IpEndpointName
	struct sockaddr_in addr;
	CFRange addrRange = { 0, sizeof(addr) };
	CFDataGetBytes(data, addrRange, (UInt8 *) &addr);
	IpEndpointName remoteEndpoint(ntohl(addr.sin_addr.s_addr), ntohs(addr.sin_port));
	
    try {
        reinterpret_cast<ILLPOSCListener*>(info)->ProcessPacket(bytes, bytesSize, remoteEndpoint);
    } catch (osc::Exception& e) {
        NSLog(@"OSC InputSocketCallback exception %s", e.what());
    }
}

void ILLPOSCListener::ProcessMessage( const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint )
{
	if (mDelegate != nil)
		[mDelegate ProcessMessage: m];
	
	try {
		//osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
		char addressString[64];
		remoteEndpoint.AddressAndPortAsString(addressString);
		//std::cout << (unsigned short) mInputPortNumber << " " << addressString << " " << m << std::endl;
		//NSLog(@"messageFromPort: %d, %s, %s", mInputPortNumber, addressString, m.AddressPattern());
	} catch(osc::Exception& e) {
		// any parsing errors such as unexpected argument types, or 
		// missing arguments get thrown as exceptions.
		NSLog(@"Could not parse OSC %s:%s", m.AddressPattern(), e.what());
	}
}

void ILLPOSCListener::CreateSocket()
{
	CFSocketContext socketContext;
	
	socketContext.version = 0;
	socketContext.info = (void *) this;
	socketContext.retain = NULL;
	socketContext.release = NULL;
	socketContext.copyDescription = NULL;
	
	mSocket = CFSocketCreate(NULL, PF_INET, SOCK_DGRAM, IPPROTO_UDP, kCFSocketDataCallBack, ILLPOSCListener::InputSocketCallback, &socketContext);
	if (!mSocket) 
	{
		NSLog(@"Failed to create socket");
		return;
	}
	else
	{
		NSLog(@"created socket");
	}
	
	mInputSocketRunLoopSource = CFSocketCreateRunLoopSource(NULL, mSocket, 0);
	CFRunLoopAddSource(CFRunLoopGetCurrent(), mInputSocketRunLoopSource, kCFRunLoopCommonModes);
	mIsSocketInitialized = false;
}

void ILLPOSCListener::DeleteInputSocket()
{
	if (mSocket) {
		CFSocketInvalidate(mSocket);
		CFRelease(mSocket);
	}
}
