/*
 *  ILLPOSCListener.h
 *  smoke_iphone
 *
 *  Created by Field Temple on 11-03-21.
 *  Copyright 2011 InteraXon. All rights reserved.
 *
 */
//  For OSC
#include "OscReceivedElements.h"
#include "OscPacketListener.h"
#include "OscOutboundPacketStream.h"
#include "IpEndpointName.h"
#include <netinet/in.h>

//  For std:: output
#include <iostream>
#include "OscPrintReceivedElements.h"

//  For Networking
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#import "OSCListenerDelegate.h"

#import <Foundation/Foundation.h>

class ILLPOSCListener : public osc::OscPacketListener 
{
public:
	//  CTOR / DTOR
	ILLPOSCListener(void);
	~ILLPOSCListener();
	
	//  Accessors
	void SetSocketPort(short portNumber);
		
	CFSocketRef GetSocket() { return mSocket; }
	
public:
	//  Internal Callback Functions (must be public)
	static void InputSocketCallback(CFSocketRef s, CFSocketCallBackType callbackType, CFDataRef address, const void *primData, void *info);

	id<OSCListenerDelegate>	mDelegate;

protected:
	//  Internal Functions
	virtual void ProcessMessage( const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint );
	void CreateSocket();
	void DeleteInputSocket();
	
	//  State
	short					mInputPortNumber;
	CFSocketRef				mSocket;
	bool					mIsSocketInitialized;
	CFRunLoopSourceRef		mInputSocketRunLoopSource;
};