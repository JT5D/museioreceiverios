//
//  MuseIOReceiverViewController.m
//  MuseIOReceiver
//
//  Created by Interaxon on 2014-09-11.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import "MuseIOReceiverViewController.h"

@interface MuseIOReceiverViewController (){
    RouterOSCMuseHeadband *oscMuse;
}
@end


@implementation MuseIOReceiverViewController

@synthesize delta1;
@synthesize delta2;
@synthesize delta3;
@synthesize delta4;
@synthesize theta1;
@synthesize theta2;
@synthesize theta3;
@synthesize theta4;
@synthesize alpha1;
@synthesize alpha2;
@synthesize alpha3;
@synthesize alpha4;
@synthesize beta1;
@synthesize beta2;
@synthesize beta3;
@synthesize beta4;
@synthesize gamma1;
@synthesize gamma2;
@synthesize gamma3;
@synthesize gamma4;

-(void) receivedEEG:(MuseEEGData *)eegData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"EEG Data: %f : %f : %f : %f", eegData->eeg[0],eegData->eeg[1],eegData->eeg[2],eegData->eeg[3]]);
}

-(void) receivedDroppedEEG:(MuseDroppedEEGData *)droppedEEGData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"EEG Dropped: %d", droppedEEGData->num]);
}

-(void) receivedEEGQuantization:(MuseQuantizationData *)quantizationData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"EEG Quantization Data: %d : %d : %d : %d", quantizationData->quant[0],quantizationData->quant[1],quantizationData->quant[2],quantizationData->quant[3]]);
}

-(void) receivedDRLRef:(MuseDRLRefData *)drlRefData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"DRL Ref Data: %f : %f", drlRefData->drl, drlRefData->ref]);
}

-(void) receivedNoiseLevels:(MuseNoiseData *)noiseData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"Noise Data: %d : %d : %d : %d", noiseData->good[0],noiseData->good[1],noiseData->good[2],noiseData->good[3]]);
}

-(void) receivedACC:(MuseAccData *)accData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"Acc Data: %f : %f : %f", accData->acc[0],accData->acc[1],accData->acc[2]]);
}

-(void) receivedDroppedACC:(MuseDroppedAccData *)droppedACCData
{
    //Logging data to verify it's received
    NSLog(@"%@", [NSString stringWithFormat:@"Acc Dropped: %d", droppedACCData->num]);
}

-(void) receivedDelta:(MuseDSPData *)deltaData
{
    [delta1 setText: [NSString stringWithFormat: @"%.2f", deltaData->dsp[0]]];
    [delta2 setText: [NSString stringWithFormat: @"%.2f", deltaData->dsp[1]]];
    [delta3 setText: [NSString stringWithFormat: @"%.2f", deltaData->dsp[2]]];
    [delta4 setText: [NSString stringWithFormat: @"%.2f", deltaData->dsp[3]]];
    //NSLog(@"%@", [NSString stringWithFormat:@"Delta Data: %f : %f : %f : %f", deltaData->dsp[0],deltaData->dsp[1],deltaData->dsp[2],deltaData->dsp[3]]);
}

-(void) receivedTheta:(MuseDSPData *)thetaData
{
    [theta1 setText: [NSString stringWithFormat: @"%.2f", thetaData->dsp[0]]];
    [theta2 setText: [NSString stringWithFormat: @"%.2f", thetaData->dsp[1]]];
    [theta3 setText: [NSString stringWithFormat: @"%.2f", thetaData->dsp[2]]];
    [theta4 setText: [NSString stringWithFormat: @"%.2f", thetaData->dsp[3]]];
//    NSLog(@"%@", [NSString stringWithFormat:@"Theta Data: %f : %f : %f : %f", thetaData->dsp[0],thetaData->dsp[1],thetaData->dsp[2],thetaData->dsp[3]]);
    
}

-(void) receivedAlpha:(MuseDSPData *)alphaData
{
    [alpha1 setText: [NSString stringWithFormat: @"%.2f", alphaData->dsp[0]]];
    [alpha2 setText: [NSString stringWithFormat: @"%.2f", alphaData->dsp[1]]];
    [alpha3 setText: [NSString stringWithFormat: @"%.2f", alphaData->dsp[2]]];
    [alpha4 setText: [NSString stringWithFormat: @"%.2f", alphaData->dsp[3]]];
//    NSLog(@"%@", [NSString stringWithFormat:@"Alpha Data: %f : %f : %f : %f", alphaData->dsp[0],alphaData->dsp[1],alphaData->dsp[2],alphaData->dsp[3]]);
    
}

-(void) receivedBeta:(MuseDSPData *)betaData
{
    [beta1 setText: [NSString stringWithFormat: @"%.2f", betaData->dsp[0]]];
    [beta2 setText: [NSString stringWithFormat: @"%.2f", betaData->dsp[1]]];
    [beta3 setText: [NSString stringWithFormat: @"%.2f", betaData->dsp[2]]];
    [beta4 setText: [NSString stringWithFormat: @"%.2f", betaData->dsp[3]]];
//    NSLog(@"%@", [NSString stringWithFormat:@"Beta Data: %f : %f : %f : %f", betaData->dsp[0],betaData->dsp[1],betaData->dsp[2],betaData->dsp[3]]);
    
}

-(void) receivedGamma:(MuseDSPData *)gammaData
{
    [gamma1 setText: [NSString stringWithFormat: @"%.2f", gammaData->dsp[0]]];
    [gamma2 setText: [NSString stringWithFormat: @"%.2f", gammaData->dsp[1]]];
    [gamma3 setText: [NSString stringWithFormat: @"%.2f", gammaData->dsp[2]]];
    [gamma4 setText: [NSString stringWithFormat: @"%.2f", gammaData->dsp[3]]];
    //NSLog(@"%@", [NSString stringWithFormat:@"Gamma Data: %f : %f : %f : %f", gammaData->dsp[0],gammaData->dsp[1],gammaData->dsp[2],gammaData->dsp[3]]);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    oscMuse = [[RouterOSCMuseHeadband alloc] init];
    [oscMuse addOSCPacketListener:self];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
