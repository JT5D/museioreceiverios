//
//  main.m
//  museioreceiver
//
//  Created by Interaxon on 2014-09-11.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MuseIOReceiverAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MuseIOReceiverAppDelegate class]));
    }
}
