//
//  MuseIOReceiverViewController.h
//  MuseIOReceiver
//
//  Created by Interaxon on 2014-09-11.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouterOSCMuseHeadband.h"
#import "OSCListener.h"

@interface MuseIOReceiverViewController : UIViewController <OSCListener>

@property (weak, nonatomic) IBOutlet UILabel *delta1;
@property (weak, nonatomic) IBOutlet UILabel *delta2;
@property (weak, nonatomic) IBOutlet UILabel *delta3;
@property (weak, nonatomic) IBOutlet UILabel *delta4;
@property (weak, nonatomic) IBOutlet UILabel *theta1;
@property (weak, nonatomic) IBOutlet UILabel *theta2;
@property (weak, nonatomic) IBOutlet UILabel *theta3;
@property (weak, nonatomic) IBOutlet UILabel *theta4;
@property (weak, nonatomic) IBOutlet UILabel *alpha1;
@property (weak, nonatomic) IBOutlet UILabel *alpha2;
@property (weak, nonatomic) IBOutlet UILabel *alpha3;
@property (weak, nonatomic) IBOutlet UILabel *alpha4;
@property (weak, nonatomic) IBOutlet UILabel *beta1;
@property (weak, nonatomic) IBOutlet UILabel *beta2;
@property (weak, nonatomic) IBOutlet UILabel *beta3;
@property (weak, nonatomic) IBOutlet UILabel *beta4;
@property (weak, nonatomic) IBOutlet UILabel *gamma1;
@property (weak, nonatomic) IBOutlet UILabel *gamma2;
@property (weak, nonatomic) IBOutlet UILabel *gamma3;
@property (weak, nonatomic) IBOutlet UILabel *gamma4;

//Receivers to implement
-(void) receivedEEG: (MuseEEGData *) eegData;
-(void) receivedDroppedEEG: (MuseDroppedEEGData *) droppedEEGData;
-(void) receivedEEGQuantization: (MuseQuantizationData *) quantizationData;
-(void) receivedNoiseLevels: (MuseNoiseData *) noiseData;
-(void) receivedACC: (MuseAccData *) accData;
-(void) receivedDroppedACC: (MuseDroppedAccData *) droppedACCData;
-(void) receivedDRLRef:(MuseDRLRefData *)drlRefData;
-(void) receivedDelta: (MuseDSPData *) deltaData;
-(void) receivedTheta: (MuseDSPData *) thetaData;
-(void) receivedAlpha: (MuseDSPData *) alphaData;
-(void) receivedBeta: (MuseDSPData *) betaData;
-(void) receivedGamma: (MuseDSPData *) gammaData;

@end
