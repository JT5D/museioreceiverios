//
//
//  MuseIOReceiverAppDelegate.h
//  MuseIOReceiver
//
//  Created by Interaxon on 2014-09-11.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MuseIOReceiverAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
