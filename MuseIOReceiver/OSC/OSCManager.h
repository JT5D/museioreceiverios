//
//  OSCRouter.h
//  MuseIOReceiver
//
//  Created by Matt Farough on 2014-9-12.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import "ILLPCocoaOSCPack.h"

ZKMDECLCPPT(ILLPOSCListener)

#import "Router.h"

@protocol RouterOSCDelegate <NSObject>

-(void) processMessage: (const osc::ReceivedMessage &) m;

@end

@interface OSCManager : ILLPCocoaOSCPack <OSCListenerDelegate>
{
	NSMutableDictionary *controlInputs;
    dispatch_queue_t oscQueue;
}

+(OSCManager *) sharedManager;

-(dispatch_queue_t) oscQueue;
-(void) resetOSCConfig;
-(void) addTrigger: (NSString *) newTrigger forDelegate: (id <RouterOSCDelegate>) delegate;
-(void) addTrigger: (NSString *) newTriffer;
-(void) setValueForControlMessage: (float) newValue controlString: (NSString *) controlString;
-(void) setDataForControlMessage: (NSData*) newData controlString: (NSString *) controlString;
-(void) setDataForControlMessage: (NSData*) newData controlString: (NSString *) controlString dataType: (NSString *) dataType;
-(void) setStringForControlMessage: (NSString*) newString controlString: (NSString *) controlString;
-(void) setBlobForControlMessage: (NSData*) newBlob controlString: (NSString *) controlString;

@end
