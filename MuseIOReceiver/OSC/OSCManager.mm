 //
//  OSCRouter.m
//  MuseIOReceiver
//
//  Created by Matt Farough on 2014-9-12.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import "OSCManager.h"

//  For OSC
#include "OscReceivedElements.h"
#include "OscPacketListener.h"
#include "OscException.h"
#include "OscOutboundPacketStream.h"
#include "IpEndpointName.h"
#include <netinet/in.h>

//  For std:: output
#include <iostream>
#include "OscPrintReceivedElements.h"

//  For Networking
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define maxBlobDataSize 1300

@implementation OSCManager
{
    bool initialized;
}

static OSCManager *sharedManager = nil;


+(OSCManager *) sharedManager
{
	@synchronized(self)
	{
		if (!sharedManager)
        {
			sharedManager = [[OSCManager alloc] init];
		}
        
		return sharedManager;
	}
	
	return nil;
}

-(id) init
{
    int oscInPort = 5000;
    int oscOutPort = 5001;
    NSString *oscOutIP = [NSString stringWithFormat: @"127.0.0.1"];
    
	self = [super init];
	
    if (!self)
		return nil;
	
	controlInputs = [NSMutableDictionary dictionaryWithCapacity: 10];
	
    initialized = false;
    NSLog(@"initializing RouterOSC... listening to: %d sending to %@ :: %d", oscInPort, oscOutIP, oscOutPort);
    
    [self initializeOSCStateWithInboundPort: oscInPort outboundIP: oscOutIP outboundPort: oscOutPort];
    
    mOSCListener->mDelegate = self;
    
    oscQueue = dispatch_queue_create("com.interaxon.osc.oscqueue", DISPATCH_QUEUE_SERIAL);
    
    initialized = true;

	return self;
}


-(void) dealloc
{
    if (mMessageData) CFRelease(mMessageData);
}

-(dispatch_queue_t) oscQueue
{
    return oscQueue;
}

-(void) resetOSCConfig
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    int oscInPort = [[prefs stringForKey: @"osc_in_port_preference"] intValue];
	if (oscInPort == 0)
        return;
    NSString *oscOutIP = [prefs stringForKey: @"osc_out_ip_preference"];
	int oscOutPort = [[prefs stringForKey: @"osc_out_port_preference"] intValue];
	bool oscEnable = [[prefs stringForKey:@"osc_enable"] boolValue];
    
    initialized = false;
    if (oscEnable)
    {
        NSLog(@"%@", [prefs stringForKey: @"osc_in_port_preference"]);
        NSLog(@"initializing RouterOSC... listening to: %d sending to %@ :: %d", oscInPort, oscOutIP, oscOutPort);
        
        [self initializeOSCStateWithInboundPort: oscInPort outboundIP: oscOutIP outboundPort: oscOutPort];
        
        mOSCListener->mDelegate = self;
        
        if ((oscInPort != 0 and oscOutPort != 0) && (oscEnable == true))
            initialized = true;
    }
}

-(void) setDataForControlMessage: (NSData*) newData controlString: (NSString *) controlString
{
    if (initialized)
    {
        dispatch_sync(oscQueue, ^(void){
            unsigned int curByte = 0;
            unsigned char * data = (unsigned char*) [newData bytes];
            while (curByte < [newData length])
            {
                [self clearMessageData];
                unsigned int curChunk = 256;
                osc::OutboundPacketStream p(mMessageBuffer, IP_MTU_SIZE);
                p.Clear();
                            
                p << osc::BeginMessage([controlString UTF8String]);
                
                NSMutableString * string = [NSMutableString stringWithFormat:@""];
                if (curByte + curChunk >= [newData length])
                    curChunk = [newData length] - curByte;
                // turn list of bytes into a string array to reduce size of OSC packet
                for(int i=curByte;i<curByte+curChunk;i++)
                {
                    [string appendFormat:@"%d,", data[i]];
                }
                p << string.UTF8String;
                curByte += curChunk;
                p << osc::EndMessage;
                CFDataAppendBytes(mMessageData, (const UInt8*) p.Data(), p.Size());
                CFSocketError err = CFSocketSendData(mOSCListener->GetSocket(), mOSCReceiverAddressData, mMessageData, 0.1);
                if (kCFSocketSuccess != err)
                {
                    NSLog(@"Failed to send OSC data %li", err);
                }
            }
        });
    }
}

-(void) setDataForControlMessage:(NSData *)newData controlString:(NSString *)controlString dataType:(NSString *)dataType
{
    //Note: Presents supports integers and floats only, strings will have their own functoin
    //newData is an NSData containing the binary of each data content
    //controlString is the osc message label. ie /dsp/scores
    //dataType is the message contents. ie iiff = int int float float
    if (initialized)
    {
        dispatch_sync(oscQueue, ^(void){
    
            [self clearMessageData];
            osc::OutboundPacketStream p(mMessageBuffer, IP_MTU_SIZE);
            p.Clear();
            p << osc::BeginMessage([controlString UTF8String]);
            
            NSRange curRange;
            curRange.location = 0;
            
            for (int i = 0; i<[dataType  length]; i++)
            {
                if([dataType characterAtIndex:i] == 'f')
                {
                    curRange.length = sizeof(float);
                    float mData;
                    
                    [newData getBytes: &mData range: curRange];
                    p << mData;
                    curRange.location += curRange.length;
                } else if([dataType characterAtIndex:i] == 'i')
                {
                    curRange.length = sizeof(int);
                    int mData;
                    
                    [newData getBytes: &mData range: curRange];
                    p << mData;
                    curRange.location += curRange.length;
                } else if([dataType characterAtIndex:i] == 'd')
                {
                    curRange.length = sizeof(double);
                    double mData;
                    
                    [newData getBytes: &mData range: curRange];
                    p << mData;
                    curRange.location += curRange.length;
                }
                
            }
            
            p << osc::EndMessage;
            CFDataAppendBytes(mMessageData, (const UInt8*) p.Data(), p.Size());
            CFSocketError err = CFSocketSendData(mOSCListener->GetSocket(), mOSCReceiverAddressData, mMessageData, 0.1);
            if (kCFSocketSuccess != err)
            {
                NSLog(@"Failed to send OSC data %li", err);
            }
        });
    }
}

-(void) setStringForControlMessage:(NSString *)newString controlString:(NSString *)controlString
{
    if (initialized)
    {
        dispatch_sync(oscQueue, ^(void){
            [self clearMessageData];
            osc::OutboundPacketStream p(mMessageBuffer, IP_MTU_SIZE);
            p.Clear();
            
            p << osc::BeginMessage([controlString UTF8String]);
            
            NSInteger maxLength = 1500;
            if ([newString length] > maxLength) {
                
                NSRange range = NSMakeRange(0, maxLength);
                
                while(true) {
                    NSString *msgToAppend = [newString substringWithRange:range];
                    p << [msgToAppend UTF8String];
                    if ([msgToAppend length] < maxLength) {
                        break;
                    }
                    range.location = range.location + maxLength;
                    range.length = MIN([newString length] - range.location, maxLength);
                }
            } else {
                p << [newString UTF8String];
            }
            p << osc::EndMessage;
            CFDataAppendBytes(mMessageData, (const UInt8*) p.Data(), p.Size());
            CFSocketError err = CFSocketSendData(mOSCListener->GetSocket(), mOSCReceiverAddressData, mMessageData, 0.1);
            if (kCFSocketSuccess != err)
            {
                NSLog(@"Failed to send OSC string %li", err);
            }
        });
    }
}


-(void) setValueForControlMessage: (float) newValue controlString: (NSString *) controlString
{
    if (initialized)
    {
        
        dispatch_sync(oscQueue, ^(void){
            [self clearMessageData];
            osc::OutboundPacketStream p(mMessageBuffer, IP_MTU_SIZE);
            p.Clear();
            
            p << osc::BeginMessage([controlString UTF8String]) << newValue << osc::EndMessage;
            CFDataAppendBytes(mMessageData, (const UInt8*) p.Data(), p.Size());
            CFSocketError err = CFSocketSendData(mOSCListener->GetSocket(), mOSCReceiverAddressData, mMessageData, 0.1);
            if (kCFSocketSuccess != err)
            {
                NSLog(@"Failed to send OSC value %li", err);
            }
        });
    }
}

-(void) setBlobForControlMessage:(NSData *)newBlob controlString:(NSString *)controlString
{
    if (initialized)
    {
        dispatch_sync(oscQueue, ^(void){
            [self clearMessageData];
            osc::OutboundPacketStream p(mMessageBuffer, IP_MTU_SIZE);
            p.Clear();
            
            if([newBlob length] > maxBlobDataSize)
            {
                int size = [newBlob length];
                char temp[maxBlobDataSize];
                NSRange range;
                range.location = 0;
                range.length = maxBlobDataSize;
                do {
                    
                    [newBlob getBytes:temp range:range];
                    NSString *tempString = [controlString stringByAppendingString: [NSString stringWithFormat: @"Part%d", range.location]];
                    
                    p << osc::BeginMessage([tempString UTF8String]) << osc::Blob(temp, range.length) << osc::EndMessage;
                    CFDataAppendBytes(mMessageData, (const UInt8*) p.Data(), p.Size());
                    CFSocketError err = CFSocketSendData(mOSCListener->GetSocket(), mOSCReceiverAddressData, mMessageData, 0.1);
                    if (kCFSocketSuccess != err)
                    {
                        NSLog(@"Failed to send OSC blob %li", err);
                    }
                    
                    [self clearMessageData];
                    p.Clear();
                    
                    range.location += range.length;
                    range.length = (range.location+range.length)<size?maxBlobDataSize:(size-range.location);
                
                } while (range.length > 0);
            }
        });
    }
}

-(void) ProcessMessage: (const osc::ReceivedMessage &) m
{
	for (NSString *curKey in [controlInputs allKeys])
	{
		if (strcmp([curKey UTF8String], m.AddressPattern()) == 0)
		{
			NSArray *delegates = [[controlInputs objectForKey: curKey] objectForKey: @"delegates"];
			for (id<RouterOSCDelegate> curDelegate in delegates)
			{
				[curDelegate processMessage: m];
			}
		}
	}
}

-(void) addTrigger:(NSString *)newTriffer
{
    
    NSLog(@"New: %@",newTriffer);
}

-(void) addTrigger: (NSString *) newTrigger forDelegate: (id<RouterOSCDelegate>) delegate
{
	NSMutableDictionary *curDict = [controlInputs objectForKey: newTrigger];
	
	if (curDict != nil)
	{
		NSMutableArray *delegatesArray = [curDict objectForKey: @"delegates"];
		if ([delegatesArray containsObject: delegate])
		{
			NSLog(@"[./ ERROR \\.] IXRouterOSC :: Same delegate already added for %@ :: ", newTrigger);
			return;
		}
		else
		{
			[delegatesArray addObject: delegate];
		}
	}
	else
	{
		curDict = [NSMutableDictionary dictionaryWithCapacity: 1];
		
		NSMutableArray *delegatesArray = [NSMutableArray arrayWithCapacity: 1];
		
		[delegatesArray addObject: delegate];
		
		[curDict setObject: delegatesArray forKey: @"delegates"];
		
		[controlInputs setObject: curDict forKey: newTrigger];
	}
}
@end
