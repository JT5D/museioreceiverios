//
//  Router.m
//  MuseIOReceiver
//
//  Created by Matt Farough on 2014-9-12.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import "Router.h"

@implementation MuseEEGData

//Defining all the different data types
-(id)copyWithZone:(NSZone *)zone
{
    MuseEEGData *copy = [[[self class] alloc] init];
    
    copy->eeg[0]    = self->eeg[0];
    copy->eeg[1]    = self->eeg[1];
    copy->eeg[2]    = self->eeg[2];
    copy->eeg[3]    = self->eeg[3];
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseDroppedEEGData
-(id)copyWithZone:(NSZone *)zone
{
    MuseDroppedEEGData *copy = [[[self class] alloc] init];
    
    copy->num    = self->num;
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseQuantizationData

-(id)copyWithZone:(NSZone *)zone
{
    MuseQuantizationData *copy = [[[self class] alloc] init];
    
    copy->quant[0]    = self->quant[0];
    copy->quant[1]    = self->quant[1];
    copy->quant[2]    = self->quant[2];
    copy->quant[3]    = self->quant[3];
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseNoiseData

-(id)copyWithZone:(NSZone *)zone
{
    MuseNoiseData *copy = [[[self class] alloc] init];
    
    copy->good[0]    = self->good[0];
    copy->good[1]    = self->good[1];
    copy->good[2]    = self->good[2];
    copy->good[3]    = self->good[3];
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseDRLRefData

-(id)copyWithZone:(NSZone *)zone
{
    MuseDRLRefData *copy = [[[self class] alloc] init];
    
    copy->drl    = self->drl;
    copy->ref    = self->ref;
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseAccData

-(id)copyWithZone:(NSZone *)zone
{
    MuseAccData *copy = [[[self class] alloc] init];
    
    copy->acc[0]    = self->acc[0];
    copy->acc[1]    = self->acc[1];
    copy->acc[2]    = self->acc[2];
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseDroppedAccData
-(id)copyWithZone:(NSZone *)zone
{
    MuseDroppedAccData *copy = [[[self class] alloc] init];
    
    copy->num    = self->num;
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end

@implementation MuseDSPData

-(id)copyWithZone:(NSZone *)zone
{
    MuseDSPData *copy = [[[self class] alloc] init];
    
    copy->dsp[0]        = self->dsp[0];
    copy->dsp[1]        = self->dsp[1];
    copy->dsp[2]        = self->dsp[2];
    copy->dsp[3]        = self->dsp[3];
    copy->timeStamp = self->timeStamp;
    
    return copy;
}
@end


@implementation Router

@synthesize tag;

-(id) init
{
	self = [super init];
	
    if (!self)
		return nil;
	
	self.tag = @"undefined";
	
	return self;
}

-(void) dealloc
{
	self.tag = @"";
}

@end
