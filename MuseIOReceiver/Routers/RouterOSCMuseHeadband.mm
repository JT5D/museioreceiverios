//
//  OSCHeadbandRouter.m
//  MuseIOReceiver
//
//  Created by Matt Farough on 2014-9-12.
//  Copyright (c) 2014 Interaxon. All rights reserved.

#import "RouterOSCMuseHeadband.h"

@implementation RouterOSCMuseHeadband

-(id) init
{
	self = [super init];
	
    if (!self)
		return nil;

    self.tag = @"oscMuseHeadband";
	
    /*
	 Messages that are Inputs (computer -> iOS):
	 Muse EEG:
	 {/muse/raw}  { f, f, f, f }
	 Muse Acc:
	 {/muse/acc}  { f, f, f }
	 DSP processing samples
	 AlphaScore
	 {/muse/dsp/elements/alpha  { f, f, f, f }
	 BetaScore
	 {/muse/dsp/elements/beta  { f, f, f, f }
	 */
	
    //Various possible data paths for data of interest
	controlInputNames[oscMuseType_eegData] = @"/muse/eeg";
	controlInputNames[oscMuseType_eegDroppedData] = @"/muse/eeg/dropped";
	controlInputNames[oscMuseType_quantizationData] = @"/muse/eeg/quantization";
	controlInputNames[oscMuseType_alphaData] = @"/muse/dsp/elements/alpha";
    controlInputNames[oscMuseType_accData] = @"/muse/acc";
    controlInputNames[oscMuseType_accDroppedData] = @"/muse/acc/dropped";
    controlInputNames[oscMuseType_noiseData] = @"/muse/dsp/elements/is_good";
	controlInputNames[oscMuseType_drlRefData] = @"/muse/drlref";
    controlInputNames[oscMuseType_deltaData] = @"/muse/dsp/elements/delta";
    controlInputNames[oscMuseType_thetaData] = @"/muse/dsp/elements/theta";
    controlInputNames[oscMuseType_alphaData] = @"/muse/dsp/elements/alpha";
    controlInputNames[oscMuseType_betaData] = @"/muse/dsp/elements/beta";
    controlInputNames[oscMuseType_gammaData] = @"/muse/dsp/elements/gamma";
    
	for (int i = 0; i < oscMuseType_numTypes; i++)
	{
		controllerBuffer[i] = [NSMutableArray arrayWithCapacity: 100];
        [[OSCManager sharedManager] addTrigger:controlInputNames[i] forDelegate:self];
;
    }

    listeners = [[NSMutableArray alloc] initWithCapacity:5];
    
	return self;
}

-(void) dealloc
{
}

-(void) processMessage: (const osc::ReceivedMessage &) m
{
	try
	{
        //Receiver osc message and parse it into the proper datatype and receiver
		osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
		
		for (int i = 0; i < oscMuseType_numTypes; i++)
		{
			if (strcmp([controlInputNames[i] UTF8String], m.AddressPattern()) == 0)
			{
				if (controllerBuffer[i].count > 99)
					[controllerBuffer[i] removeObjectAtIndex: 0];
				
				if (i == oscMuseType_eegData)
				{
					MuseEEGData *eegEntry = [[MuseEEGData alloc] init];
                    float eegVal[4];
					args >> eegVal[0] >> eegVal[1] >> eegVal[2] >> eegVal[3] >> osc::EndMessage;
                    
                    for(int i = 0; i<4; i++)
                    {
                        eegEntry->eeg[i] = eegVal[i];
					}
                    
                    [self broadcastOSCPacket:eegEntry];
        		}
				else if (i == oscMuseType_eegDroppedData)
				{
					MuseDroppedEEGData *eegDroppedEntry = [[MuseDroppedEEGData alloc] init];
                    osc::int32 eegDroppedVal;
					
                    args >> eegDroppedVal >> osc::EndMessage;
                    
                    eegDroppedEntry->num = eegDroppedVal;
                    
                    [self broadcastOSCPacket:eegDroppedEntry];
        		}
				else if (i == oscMuseType_quantizationData)
				{
					MuseQuantizationData *eegQuantizationEntry = [[MuseQuantizationData alloc] init];
                    osc::int32 quantization[4];
					args >> quantization[0] >> quantization[1] >> quantization[2] >> quantization[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        eegQuantizationEntry->quant[i] = quantization[i];
					}
        			
                    [self broadcastOSCPacket:eegQuantizationEntry];
            	}
                else if (i == oscMuseType_accData)
				{
					MuseAccData *accEntry = [[MuseAccData alloc] init];
                    float accVal[3];
					args >> accVal[0] >> accVal[1] >> accVal[2] >> osc::EndMessage;
                    
                    for(int i = 0; i<3; i++)
                    {
                        accEntry->acc[i] = accVal[i];
					}
                    [self broadcastOSCPacket:accEntry];
                }
				else if (i == oscMuseType_accDroppedData)
				{
					MuseDroppedAccData *accDroppedEntry = [[MuseDroppedAccData alloc] init];
                    osc::int32 accDroppedVal;
					
                    args >> accDroppedVal >> osc::EndMessage;
                    
                    accDroppedEntry->num = accDroppedVal;
                    [self broadcastOSCPacket:accDroppedEntry];
        		}
				else if (i == oscMuseType_noiseData)
				{
					MuseEEGData *eegQualityIndicatorEntry = [[MuseEEGData alloc] init];
                    osc::int32 signalQuality[4];
					args >> signalQuality[0] >> signalQuality[1] >> signalQuality[2] >> signalQuality[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        eegQualityIndicatorEntry->eeg[i] = signalQuality[i];
					}
        			
                    [self broadcastOSCPacket:eegQualityIndicatorEntry];
            	}
                else if (i == oscMuseType_drlRefData)
				{
					MuseDRLRefData *drlRefEntry = [[MuseDRLRefData alloc] init];
                    float drlRef[2];
					args >> drlRef[0] >> drlRef[1] >> osc::EndMessage;
                    drlRefEntry->drl = drlRef[0];
                    drlRefEntry->ref = drlRef[1];
        			
                    [self broadcastOSCPacket:drlRefEntry];
            	}
                else if (i == oscMuseType_deltaData)
				{
					MuseDSPData *dspEntry = [[MuseDSPData alloc] init];
                    float delta[4];
					args >> delta[0] >> delta[1] >> delta[2] >> delta[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        dspEntry->dsp[i] = delta[i];
					}
                    
                    [self broadcastOSCDSPPacket:dspEntry : @"delta"];
            	}
                else if (i == oscMuseType_thetaData)
				{
					MuseDSPData *dspEntry = [[MuseDSPData alloc] init];
                    float theta[4];
					args >> theta[0] >> theta[1] >> theta[2] >> theta[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        dspEntry->dsp[i] = theta[i];
					}
        			
                    [self broadcastOSCDSPPacket:dspEntry : @"theta"];
            	}
                else if (i == oscMuseType_alphaData)
				{
					MuseDSPData *dspEntry = [[MuseDSPData alloc] init];
                    float alpha[4];
					args >> alpha[0] >> alpha[1] >> alpha[2] >> alpha[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        dspEntry->dsp[i] = alpha[i];
					}
        			
                    [self broadcastOSCDSPPacket:dspEntry : @"alpha"];
            	}
                else if (i == oscMuseType_betaData)
				{
					MuseDSPData *dspEntry = [[MuseDSPData alloc] init];
                    float beta[4];
					args >> beta[0] >> beta[1] >> beta[2] >> beta[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        dspEntry->dsp[i] = beta[i];
					}
        			
                    [self broadcastOSCDSPPacket:dspEntry : @"beta"];
            	}
                else if (i == oscMuseType_gammaData)
				{
					MuseDSPData *dspEntry = [[MuseDSPData alloc] init];
                    float gamma[4];
					args >> gamma[0] >> gamma[1] >> gamma[2] >> gamma[3] >> osc::EndMessage;
                    for(int i = 0; i<4; i++)
                    {
                        dspEntry->dsp[i] = gamma[i];
					}
        			
                    [self broadcastOSCDSPPacket:dspEntry : @"gamma"];
            	}
                else
				{
					NSLog(@"undetermined message :: %s", m.AddressPattern());
				}
				
				break;
			}
		}
	}
	catch(osc::Exception& e )
	{
		std::cout << "error while parsing message: "
		<< m.AddressPattern() << ": " << e.what() << "\n";
	}
}

-(void) addOSCPacketListener: (id<OSCListener>) listener
{
    //Adding new listener to receiver data broadcasts
    [listeners addObject:listener];
}

-(void) broadcastOSCPacket:(NSObject *)packet
{
    //Set receiver of raw data depending on type
    for (id<OSCListener> listener in listeners) {
        if ([packet isMemberOfClass: [MuseEEGData class]])
        {
            if ([listener respondsToSelector:@selector(receivedEEG:)])
            {
                MuseEEGData *eegData = (MuseEEGData *) packet;
                [listener receivedEEG:eegData];
            }
        }
        else if ([packet isMemberOfClass: [MuseDroppedEEGData class]])
        {
            if([listener respondsToSelector:@selector(receivedDroppedEEG:)])
            {
                MuseDroppedEEGData *droppedEEGData = (MuseDroppedEEGData *) packet;
                [listener receivedDroppedEEG: droppedEEGData];
            }
        }else if ([packet isMemberOfClass: [MuseQuantizationData class]])
        {
            if([listener respondsToSelector:@selector(receivedEEGQuantization:)])
            {
                MuseQuantizationData *quantizationData = (MuseQuantizationData *) packet;
                [listener receivedEEGQuantization: quantizationData];
            }
        }else if ([packet isMemberOfClass: [MuseAccData class]])
        {
            if([listener respondsToSelector:@selector(receivedACC:)])
            {
                MuseAccData *accData = (MuseAccData *) packet;
                [listener receivedACC:accData];
            }
        }
        else if ([packet isMemberOfClass: [MuseDroppedAccData class]])
        {
            if([listener respondsToSelector:@selector(receivedDroppedACC:)])
            {
                MuseDroppedAccData *droppedAccData = (MuseDroppedAccData *) packet;
                [listener receivedDroppedACC: droppedAccData];
            }
        }else if ([packet isMemberOfClass: [MuseNoiseData class]])
        {
            if([listener respondsToSelector:@selector(receivedNoiseLevels:)])
            {
                MuseNoiseData *noiseData = (MuseNoiseData *) packet;
                [listener receivedNoiseLevels: noiseData];
            }
        }else if ([packet isMemberOfClass: [MuseDRLRefData class]])
        {
            if([listener respondsToSelector:@selector(receivedDRLRef:)])
            {
                MuseDRLRefData *drlRefData = (MuseDRLRefData *) packet;
                [listener receivedDRLRef: drlRefData];
            }
        }
    }
}

-(void) broadcastOSCDSPPacket:(MuseDSPData *) data : (NSString *) dataType
{
    //Selecting receiver of dsp data depending on type
    for (id<OSCListener> listener in listeners) {
        if ([dataType isEqualToString:@"delta"])
        {
            if ([listener respondsToSelector:@selector(receivedDelta:)])
            {
                MuseDSPData *deltaData = (MuseDSPData *) data;
                [listener receivedDelta: deltaData];
            }
        }
        else if ([dataType isEqualToString:@"theta"])
        {
            if ([listener respondsToSelector:@selector(receivedTheta:)])
            {
                MuseDSPData *thetaData = (MuseDSPData *) data;
                [listener receivedTheta: thetaData];
            }
        }
        else if ([dataType isEqualToString:@"alpha"])
        {
            if ([listener respondsToSelector:@selector(receivedAlpha:)])
            {
                MuseDSPData *alphaData = (MuseDSPData *) data;
                [listener receivedAlpha: alphaData];
            }
        }
        else if ([dataType isEqualToString:@"beta"])
        {
            if ([listener respondsToSelector:@selector(receivedBeta:)])
            {
                MuseDSPData *betaData = (MuseDSPData *) data;
                [listener receivedBeta: betaData];
            }
        }
        else if ([dataType isEqualToString:@"gamma"])
        {
            if ([listener respondsToSelector:@selector(receivedGamma:)])
            {
                MuseDSPData *gammaData = (MuseDSPData *) data;
                [listener receivedGamma: gammaData];
            }
        }
    }
}


@end
