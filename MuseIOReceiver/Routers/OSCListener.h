//
//  IXOSCListener.h
//  MuseIOReceiver
//
//  Created by James Moon on 2014-01-28.
//
//

#import <Foundation/Foundation.h>
#import "Router.h"

@protocol OSCListener <NSObject>
@required

//Definition of all the different types of data receivers
-(void) receivedEEG: (MuseEEGData *) eegData;
-(void) receivedDroppedEEG: (MuseDroppedEEGData *) droppedEEGData;
-(void) receivedEEGQuantization: (MuseQuantizationData *) quantizationData;
-(void) receivedNoiseLevels: (MuseNoiseData *) noiseData;
-(void) receivedACC: (MuseAccData *) accData;
-(void) receivedDRLRef: (MuseDRLRefData *) drlRefData;
-(void) receivedDroppedACC: (MuseDroppedAccData *) droppedACCData;
-(void) receivedDelta: (MuseDSPData *) deltaData;
-(void) receivedTheta: (MuseDSPData *) thetaData;
-(void) receivedAlpha: (MuseDSPData *) alphaData;
-(void) receivedBeta: (MuseDSPData *) betaData;
-(void) receivedGamma: (MuseDSPData *) gammaData;

@end
