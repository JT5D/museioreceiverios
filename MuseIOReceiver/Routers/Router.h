//
//  Router.h
//  MuseIOReceiver
//
//  Created by Matt Farough on 2014-9-12.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MuseEEGData : NSObject <NSCopying>
{
@public
    float eeg[4];
	double timeStamp;
}
@end


@interface MuseDroppedEEGData : NSObject <NSCopying>
{
@public
    int num;
	double timeStamp;
}
@end

@interface MuseAccData : NSObject <NSCopying>
{
@public
    float acc[3];
	double timeStamp;
}

@end

@interface MuseDroppedAccData : NSObject <NSCopying>
{
@public
    int num;
	double timeStamp;
}

@end

@interface MuseQuantizationData : NSObject <NSCopying>
{
@public
    int quant[4];
	double timeStamp;
}

@end

@interface MuseNoiseData : NSObject <NSCopying>
{
@public
    bool good[4];
	double timeStamp;
}

@end

@interface MuseDRLRefData : NSObject <NSCopying>
{
@public
    float drl;
    float ref;
  	double timeStamp;
}

@end

@interface MuseDSPData : NSObject <NSCopying>
{
@public
    float dsp[4];
    double timeStamp;
}
@end

@interface Router : NSObject
{
	NSString *tag;
}

@property(readwrite, retain) NSString *tag;

@end
