//
//  RouterOSCHeadsetRouter.h
//  MuseIOReceiver
//
//  Created by Matt Farough on 2014-9-12.
//  Copyright (c) 2014 Interaxon. All rights reserved.
//

#import "Router.h"
#import "OSCManager.h"
#import "OSCListener.h"

typedef enum _oscMuseType
{
	oscMuseType_eegData = 0,
    oscMuseType_eegDroppedData,
	oscMuseType_quantizationData,
	oscMuseType_accData,
    oscMuseType_accDroppedData,
    oscMuseType_noiseData,
    oscMuseType_drlRefData,
    oscMuseType_deltaData,
    oscMuseType_thetaData,
    oscMuseType_alphaData,
    oscMuseType_betaData,
    oscMuseType_gammaData,
    oscMuseType_numTypes,
} OSCMuseType;


@interface RouterOSCMuseHeadband : Router <RouterOSCDelegate>
{
	NSMutableDictionary *mDataDict;
	
	NSString *controlInputNames[oscMuseType_numTypes];
	
	unsigned int controlBufferCount[oscMuseType_numTypes];
	NSMutableArray *controllerBuffer[oscMuseType_numTypes];
    
    //Data listeners
    NSMutableArray *listeners;

}

-(void) addOSCPacketListener: (id<OSCListener>) listener;
@end
